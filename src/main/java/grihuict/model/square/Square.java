package grihuict.model.square;

import grihuict.model.board.Player;

public interface Square {
    String checkAndExecute(Player player);
    int getSquarePosition();

}
