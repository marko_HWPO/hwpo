package grihuict.model.square;


import grihuict.model.board.Player;

public class LosePointsSquare implements Square {
    int position;
    int point;

    public LosePointsSquare(int position, int point) {
        this.position = position;
        this.point = point;
    }

    @Override
    public String checkAndExecute(Player player) {
        int newPoints;
        newPoints = player.getPoints() - point;
        player.setPoints(newPoints);
        return  "Lose Points Square -" + point + " points.\n";

    }

    @Override
    public int getSquarePosition() {
        return position;
    }
}
