package grihuict.model.square;

import grihuict.model.board.PlayerTurnOptions;
import grihuict.model.board.Player;

public class CardSquare implements Square {
    int position;
    Card cardColour;

    public CardSquare(int position,Card cardColour) {
        this.position = position;
        this.cardColour = cardColour;
    }

    @Override
    public String checkAndExecute(Player player) {
        switch (cardColour){
            case RED:
                player.setRedCardNumber(1);
            case YELLOW:
                player.setYellowCardNumber(player.getYellowCardNumber()+1);
        }
    if (player.getYellowCardNumber() == 2 || player.getRedCardNumber() == 1 ){
        player.setPlayerTurnOptions(PlayerTurnOptions.REMOVE);
        player.setRedCardNumber(1);
        return "You are being removed from game because you gathered "+player.getRedCardNumber()+" Red Cards\n";
    }
    else{
        return "Card Square \nBe careful you have "+ player.getYellowCardNumber()+" yellow Cards!! \n";
    }
    }

    @Override
    public int getSquarePosition() {

        return position;
    }
}
