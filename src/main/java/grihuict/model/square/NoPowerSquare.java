package grihuict.model.square;

import grihuict.model.board.Player;

public class NoPowerSquare implements Square {
    int position;

    public NoPowerSquare(int position) {
        this.position = position;
    }

    @Override
    public String checkAndExecute(Player player) {
        return "Normal Square\n";
    }

    @Override
    public int getSquarePosition() {

        return position;
    }

    public String toString() {
        return "Position:" + position;
    }
}
