package grihuict.model.square;

import grihuict.model.board.Player;

public class ForwardSquare implements Square {
    int position;
    int powerUp;

    public ForwardSquare(int position, int powerUp) {
        this.position = position;
        this.powerUp = powerUp;
    }

    @Override
    public String checkAndExecute(Player player) {
        player.setCurrentSquarePosition(player.getCurrentSquarePositionPosition() + powerUp);
        return  "Power Square \nClimb +" + powerUp + " positions\n";
    }

    @Override
    public int getSquarePosition() {

        return position;
    }

    public String toString() {
        return "Position:" + position + " PowerUp=" + powerUp;
    }
}
