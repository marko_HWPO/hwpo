package grihuict.model.board;

import java.util.Random;

public class NormalDice implements Dice {

    @Override
    public int rollDice() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(6) + 1;
    }
}
