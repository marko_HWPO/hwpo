package grihuict.model.board;

public class Player {
    private int currentSquarePosition;
    private int diceNumber;
    private String name;
    private int points = 0;
    private PlayerTurnOptions playerTurnOptions = PlayerTurnOptions.PLAY;
    private int yellowCardNumber=0;
    private int redCardNumber=0;

    public Player(int currentSquarePosition, String name) {
        this.currentSquarePosition = currentSquarePosition;
        this.name = name;
    }

    public PlayerTurnOptions getPlayerTurnOptions() {
        return playerTurnOptions;
    }

    public void setPlayerTurnOptions(PlayerTurnOptions playerTurnOptions) {
        this.playerTurnOptions = playerTurnOptions;
    }

    public int getRedCardNumber() {
        return redCardNumber;
    }

    public void setRedCardNumber(int redCardNumber) {
        this.redCardNumber = redCardNumber;
    }


    public String getName() {
        return name;
    }

    public void setCurrentSquarePosition(int currentSquarePosition) {
        this.currentSquarePosition = currentSquarePosition;
    }

    public int getCurrentSquarePositionPosition() {
        return currentSquarePosition;
    }

    public int getDiceNumber() {
        return diceNumber;
    }

    public int rollDice() {
        Dice normalDice = new NormalDice();
        diceNumber = normalDice.rollDice();
        return diceNumber;
    }

    public int getYellowCardNumber() {
        return yellowCardNumber;
    }

    public void setYellowCardNumber(int yellowCardNumber) {
        this.yellowCardNumber = yellowCardNumber;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String toString() {
        return "Name " + name + ", Position:" + currentSquarePosition;
    }
}
