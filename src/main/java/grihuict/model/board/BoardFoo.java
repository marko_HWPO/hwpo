package grihuict.model.board;

import grihuict.model.square.Square;

import java.util.ArrayList;
import java.util.List;

import static grihuict.model.board.FooFinishNumber.End;


public class BoardFoo implements Board {
    private List<Square> squareList = new ArrayList<>();
    private PlayerList playerList = new PlayerList();


    @Override
    public void setPlayerList(List<Player> players1){
        playerList.setPlayerList(players1);
    }

    @Override
    public void setSquareList(List<Square> squareList) {
        this.squareList = squareList;
    }

    @Override
    public Player startGame() {
        return whoPlaysFirst();
    }

    @Override
    public Result move(int diceNumber, Player p) {

        if(diceNumber+p.getCurrentSquarePositionPosition()>squareList.size())
            p.setCurrentSquarePosition((p.getCurrentSquarePositionPosition()+diceNumber)-squareList.size());
        else
            p.setCurrentSquarePosition((diceNumber + p.getCurrentSquarePositionPosition()));

        String moveResult = squareList.get(p.getCurrentSquarePositionPosition() - 1).checkAndExecute(p);
        FooFinishNumber fooFinishNumber = End;
        Result result = p.getPoints() < fooFinishNumber.getNumber() ?
                new Result(Status.CONTINUE,  moveResult + p.getName() + " rolled " + p.getDiceNumber()+"\n"+p.getName()+" after rolling "+p.getDiceNumber()+" you are in "+p.getCurrentSquarePositionPosition()+" position with "+p.getPoints()+" points.\n")
                :
                new Result(Status.FINISH, p.getName() + " rolled " + p.getDiceNumber() + " WON!!");
        return (result);
    }

    @Override
    public Player nextPlayer(Player player) {
        Player playerToPlay;
        playerToPlay = playerList.getNextPlayer(player);
        return playerToPlay;
    }

    @Override
    public Player whoPlaysFirst() {
        return playerList.whoPlaysFirst();
    }
}
