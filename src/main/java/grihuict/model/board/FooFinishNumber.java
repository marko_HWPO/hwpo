package grihuict.model.board;

public enum FooFinishNumber {
    End(300);

    private final int number;

    FooFinishNumber(int number) {
        this.number=number;
    }

    public int getNumber() {
        return number;
    }
}
