package grihuict.model.board;

import java.util.Scanner;

import static grihuict.model.board.GameName.*;

public class  ChooseGame {
     public GameName Display(){
         String name;
         Scanner scanner=new Scanner(System.in);
         boolean condition=false;
         GameName game = null;
         do {
             System.out.println("===========\n===Games===\n===========\nPress-> Snake\nOr-> Foo\n______________________________\nIf you want to exit press Exit\n______________________________");
             name=scanner.nextLine();
             if(name.equalsIgnoreCase(Snake.toString())) {
                 game = Snake;
                 condition=true;
             }
             else if(name.equalsIgnoreCase(Foo.toString())) {
                 game = Foo;
                 condition=true;
             }
             else if (name.equalsIgnoreCase(Exit.toString())) {
                 game = Exit;
                 condition=true;
             }
         }while(!condition);
         return game;
     }
}
