package grihuict.model.json;

import grihuict.model.board.Player;
import grihuict.model.square.Square;

import java.util.List;

public interface JSONRead {

     void readFromJSON();
     List<Player> getPlayers();
     List<Square> getSquares();

}
