package grihuict.model.json;

import grihuict.model.board.Player;
import grihuict.model.square.BackwardSquare;
import grihuict.model.square.ForwardSquare;
import grihuict.model.square.NoPowerSquare;
import grihuict.model.square.Square;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class JSONSnakeRead implements JSONRead {
    private List<Player> players = new ArrayList<>();
    private  List<Square> squares = new ArrayList<>();


    @Override
    public void readFromJSON()  {
        JSONParser jsonParser = new JSONParser();
        String pathOfGameInfo="C:\\Users\\msn4l\\IdeaProjects\\SnakeBoard\\src\\\\main\\java\\grihuict\\model\\json\\GameInfo.json";

        try (FileReader fileReader = new FileReader(pathOfGameInfo)) {
            Object obj = jsonParser.parse(fileReader);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray jsonArray = (JSONArray) jsonObject.get("Players");
            for (Object o : jsonArray) {
                JSONObject jsonObject1 = (JSONObject) o;
                players.add(new Player((int) (long) (jsonObject1.get("position")), (String) jsonObject1.get("name")));
            }
            JSONArray jsonArray2 = (JSONArray) jsonObject.get("NoPowerSquares");
            for (Object o : jsonArray2) {
                JSONObject jsonObject2 = (JSONObject) o;
                squares.add(new NoPowerSquare((int) (long) (jsonObject2.get("position"))));
            }
            JSONArray jsonArray3 = (JSONArray) jsonObject.get("ForwardSquares");
            for (Object o : jsonArray3) {
                JSONObject jsonObject3 = (JSONObject) o;
                squares.add(new ForwardSquare((int) (long) (jsonObject3.get("position")), (int) (long) jsonObject3.get("power")));
            }
            JSONArray jsonArray4 = (JSONArray) jsonObject.get("BackWardSquares");
            for (Object o : jsonArray4) {
                JSONObject jsonObject4 = (JSONObject) o;
                squares.add(new BackwardSquare((int) (long) (jsonObject4.get("position")), (int) (long) jsonObject4.get("power")));
            }
            squares.sort(Comparator.comparing(Square::getSquarePosition));

        } catch (ParseException | IOException e) {
            System.out.println("An unexpected error occurred please contact our support team!");
            System.exit(0);
            e.printStackTrace();
        }
    }

    @Override
    public List<Player> getPlayers() {
        return players;
    }

    @Override
    public List<Square> getSquares() {
        return squares;
    }



}
